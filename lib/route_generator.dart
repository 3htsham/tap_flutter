import 'package:flutter/material.dart';
import 'package:tap/config/routes.dart';
import 'package:tap/src/views/passions.dart';
import 'package:tap/src/views/profile.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch(settings.name) {
      case Routes.profile:
        return MaterialPageRoute(
            builder: (_) => ProfileWidget()
        );
      default:
        return MaterialPageRoute(
          builder: (_) => PassionsWidget()
        );
    }
  }
}