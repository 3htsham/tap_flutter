import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:redux/redux.dart';
import 'package:tap/config/app_colors.dart';
import 'package:tap/route_generator.dart';
import 'package:tap/src/redux/app_State.dart';
import 'package:tap/src/redux/reducers.dart';
import 'package:tap/src/redux/user_state.dart';

import 'config/routes.dart';

void main() async {
  GlobalConfiguration().loadFromMap({
    "base_url": "https://api.zipconnect.app/",
    "api_base_url": "https://api.zipconnect.app/api/",
    "image_base_url": "https://api.zipconnect.app/img/interests/"
  });
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // TODO: Perform any action needed
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final store = Store<UserState>(
      appReducers,
      initialState: UserState.initialState(),
    );

    return StoreProvider(
        store: store,
        child: MaterialApp(
          title: "TAP",
          initialRoute: Routes.main,
          onGenerateRoute: RouteGenerator.generateRoute,
          debugShowCheckedModeBanner: false,
          builder: (context, child) {
            var query = MediaQuery.of(context);

            return MediaQuery(
              data: query.copyWith(
                // textScaleFactor: 1.0,
                devicePixelRatio: 1.0,
              ),
              child: Theme(
                child: child!,
                data: ThemeData(
                  appBarTheme: AppBarTheme(
                    brightness: Brightness.dark,
                  ),
                  brightness: Brightness.light,
                  scaffoldBackgroundColor: AppColors().scaffoldColor(1),
                  primaryColor: AppColors().scaffoldColor(1),
                  primaryColorDark: AppColors().primaryColor(1),
                  accentColor: AppColors().accentColor(1),
                  focusColor: AppColors().focusColor(1),
                  hintColor: AppColors().focusColor(1),
                  fontFamily: 'Helvetica',
                  textTheme: TextTheme(
                    headline1: TextStyle(
                        fontSize: 32.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w300,
                        color: AppColors().bodyTextColor(1)),
                    headline2: TextStyle(
                        fontSize: 24.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w400,
                        color: AppColors().bodyTextColor(1)),
                    headline3: TextStyle(
                        fontSize: 22.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w400,
                        color: AppColors().bodyTextColor(1)),
                    headline4: TextStyle(
                        fontSize: 20.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w400,
                        color: AppColors().bodyTextColor(1)),
                    headline5: TextStyle(
                        fontSize: 16.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w400,
                        color: AppColors().bodyTextColor(1)),
                    headline6: TextStyle(
                        fontSize: 16.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w400,
                        color: AppColors().bodyTextColor(1)),
                    subtitle1: TextStyle(
                        fontSize: 18.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w800,
                        color: AppColors().bodyTextColor(1)),
                    subtitle2: TextStyle(
                        fontSize: 15.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w800,
                        color: AppColors().bodyTextColor(1)),
                    bodyText1: TextStyle(
                        fontSize: 15.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w400,
                        color: AppColors().bodyTextColor(1)),
                    bodyText2: TextStyle(
                        fontSize: 14.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w400,
                        color: AppColors().bodyTextColor(1)),
                    caption: TextStyle(
                        fontSize: 12.0 * MediaQuery.textScaleFactorOf(context),
                        fontWeight: FontWeight.w500,
                        color: AppColors().captionColor(1)),
                  ),
                ),
              ),
            );
          },
        ));
  }
}
