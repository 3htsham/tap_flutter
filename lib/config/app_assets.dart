class AppAssets {

  static const background = 'assets/images/background.png';
  static const dislikeIcon = 'assets/icons/dislike.png';
  static const heartIcon = 'assets/icons/heart.png';

}