import 'package:flutter/material.dart';

class AppColors {
  Color _mainColor = Color(0xFFC2221B);
  Color _primaryColor = Color(0xFFEF9351);
  Color _secondColor = Color(0xFFA20A1D);///
  Color _secondLightColor = Color(0xFFB53B4A);///
  Color _accentColor = Color(0xFF5F1553);///
  Color _scaffoldColor = Color(0xFF0226B2);///
  static Color scaffoldColorLight = Color(0xFF4B6FFF);///
  Color _focusColor = Color(0xFF495896);
  Color _captionColor = Color(0xFF969BA7);
  Color _bodyTextColor = Color(0xFFFFFFFF);///

  static Color buttonDark = Color(0xFF081C71);///
  static Color buttonSemiDark = Color(0xFF2699FB);///
  static Color buttonLight = Color(0xFFDEDEDE);///

  static Color gradient1 = Color(0xFF4BB2FF);
  static Color gradient2 = Color(0xFF00209B);

  static Color blueDark = Color(0xFF042093);
  static Color blueLight = Color(0xFF0032FF);


  Color mainColor(double opacity) {
    return this._mainColor.withOpacity(opacity);
  }

  Color primaryColor(double opacity) {
    return _primaryColor.withOpacity(opacity);
  }

  Color secondColor(double opacity) {
    return this._secondColor.withOpacity(opacity);
  }

  Color secondLightColor(double opacity) {
    return this._secondLightColor.withOpacity(opacity);
  }

  Color accentColor(double opacity) {
    return this._accentColor.withOpacity(opacity);
  }

  Color scaffoldColor(double opacity) {
    return _scaffoldColor.withOpacity(opacity);
  }

  Color focusColor(double opacity) {
    return _focusColor.withOpacity(opacity);
  }

  Color captionColor(double opacity) {
    return _captionColor.withOpacity(opacity);
  }

  Color bodyTextColor(double opacity) {
    return _bodyTextColor.withOpacity(opacity);
  }
}