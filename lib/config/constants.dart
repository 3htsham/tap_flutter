class S {
  static const passions = 'Passions';
  static const whatAreYouInTo = 'What are you into?';
  static const pickAtLeast5 = 'Pick at least 5';

  static const continueStr = 'Continue';
  static const report = 'Report';
  static const unPair = 'Unpair';
  static const block = 'Block';
  static const instagramPosts = 'Instagram Posts';

  static const personalInfo = 'Personal Info';
  static const lookingFor = 'Looking For';
  static const relationshipStatus = 'Relationship Status';
  static const kids = 'Kids';
  static const workTitle = 'Work Title';
  static const education = 'Education';
  static const hairColor = 'Hair Color';
  static const eyeColor = 'Eye Color';
  static const height = 'Height';
  static const ethnicity = 'Ethnicity';
  static const religion = 'Religion';

  static const basicInfo = 'Basic Info';
  static const name = 'Name';
  static const gender = 'Gender';
  static const age = 'Age';
  static const location = 'Location';

  static const personName = 'Person Name';
  static const profileDescription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. ';

}