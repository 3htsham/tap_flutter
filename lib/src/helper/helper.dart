class Helper  {
  static getCategoriesData(Map<String, dynamic> data) {
    return data['data'] ?? {};
  }

  static getProfileData(Map<String, dynamic> data) {
    return data['data'] != null
        ?  data['data']['profile'] != null
          ? data['data']['profile']
          : null
          : null;
  }
}