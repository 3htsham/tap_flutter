import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:tap/config/app_colors.dart';
import 'package:tap/src/models/interest.dart';


class ScatterItem extends StatelessWidget {
  ScatterItem({this.isSelected = false, this.interest, this.onTap});
  final bool isSelected;

  final Interest? interest;
  final VoidCallback? onTap;


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    if(this.isSelected) {
      print('${this.interest!.name} : selected');
    }

    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Container(
        // height: 200,
        // constraints: BoxConstraints(
        //   maxHeight: 200
        // ),
        padding: EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: (!this.isSelected) && (this.interest != null && this.interest!.image.toString().length > 1) ? DecorationImage(
                image: NetworkImage('${GlobalConfiguration().getValue('image_base_url')}${this.interest!.image}'),
                fit: BoxFit.cover
            ) : null,
            gradient: LinearGradient(
                colors: [
                  AppColors.gradient1,
                  AppColors.gradient2
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter)
        ),
        child: InkWell(
          onTap: (){
            this.onTap?.call();
          },
          child: Container(
            constraints: BoxConstraints(
                minHeight: 200
            ),
            // height: 110,
            // width: 110,
            // color: AppColors().focusColor(0.5),
            child: Center(
              child: FittedBox(
                child: Text(
                  this.interest?.name ?? '',
                  style: textTheme.bodyText1,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

