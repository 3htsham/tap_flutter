import 'package:flutter/material.dart';

class MySolidButton extends StatelessWidget {
  final VoidCallback? onTap;
  final Color color;
  final String text;

  const MySolidButton({this.onTap, this.color = Colors.transparent, this.text = ''});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3),
          color: this.color),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: (){
            this.onTap?.call();
          },
          child: Container(
            height: 53,
            child: Center(
              child: Text(
                text.toUpperCase(),
                style: textTheme.subtitle2,
              ),
            ),
          ),
        ),
      ),
    );
  }
}