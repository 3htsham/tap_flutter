import 'package:flutter/material.dart';
import 'package:tap/config/app_colors.dart';


class MyIconButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String icon;
  final size;

  const MyIconButton(this.icon, this.size, {this.onTap});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Container(
        height: 48,
        width: 48,
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              AppColors.blueDark,
              AppColors.blueLight.withOpacity(0.33)
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: (){
              this.onTap?.call();
            },
            child: Center(
              child: ImageIcon(AssetImage(icon), color: Colors.white, size: 22,),
            ),
          ),
        ),
      ),
    );
  }
}