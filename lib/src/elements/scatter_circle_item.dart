import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:tap/src/models/interest.dart';

class ScatterCircleItem extends StatelessWidget {
  ScatterCircleItem(this.index, this.interest, {this.height = 110, this.width = 110});
  final int index;
  final Interest interest;
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    final circleSize = size.width / 2.5;

    return Container(
      height: this.height,
      width: this.width,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Theme.of(context).scaffoldBackgroundColor,
          image: (this.interest != null && this.interest.image.toString().length > 1) ? DecorationImage(
              image: NetworkImage('${GlobalConfiguration().getValue('image_base_url')}${this.interest.image}'),
              fit: BoxFit.cover
          ) : null
      ),
      child: Center(
          child: Text(
            '${this.interest.name}',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1,
          )),
    );
  }
}