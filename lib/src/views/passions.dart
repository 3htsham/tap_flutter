import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_scatter/flutter_scatter.dart';
import 'package:tap/config/app_assets.dart';
import 'package:tap/config/app_colors.dart';
import 'package:tap/config/constants.dart';
import 'package:tap/config/routes.dart';
import 'package:tap/src/elements/scatter_item.dart';
import 'package:tap/src/redux/actions.dart';
import 'package:tap/src/redux/models/view_model.dart';
import 'package:tap/src/redux/user_state.dart';

class PassionsWidget extends StatefulWidget {
  const PassionsWidget({Key? key}) : super(key: key);

  @override
  _PassionsWidgetState createState() => _PassionsWidgetState();
}

class _PassionsWidgetState extends State<PassionsWidget> {

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      body: StoreConnector<UserState, ViewModel>(
        onInit: (store) => store.dispatch(fetchPassions(store)),
        converter: (store) => ViewModel.create(store),
        builder: (context, ViewModel viewModel) {
          return Container(
            height: size.height,
            width: size.width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AppAssets.background), fit: BoxFit.cover)),
            child: Column(
              children: [
                _buildAppBar(textTheme),

                //55 Height
                SizedBox(
                  height: 55,
                ),

                Text(
                  S.whatAreYouInTo,
                  style: textTheme.headline1,
                ),
                //22 Height
                SizedBox(
                  height: 22,
                ),
                Text(
                  S.pickAtLeast5,
                  style: textTheme.bodyText1,
                ),
                SizedBox(
                  height: 15,
                ),

                Expanded(
                  child: viewModel.interestList != null &&
                      viewModel.interestList!.interests.isNotEmpty
                      ? _buildInterestsList(viewModel)
                      : SizedBox(),
                ),

                FractionallySizedBox(
                  widthFactor: 320 / 414,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 28),
                    child: Divider(
                      thickness: 0.5,
                      color: Colors.white,
                    ),
                  ),
                ),

                _buildButton(theme, textTheme, viewModel),

                SizedBox(
                  height: 54,
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildAppBar(TextTheme textTheme) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
      title: Text(
        S.passions.toUpperCase(),
        style: textTheme.subtitle2,
      ),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
    );
  }

  Widget _buildInterestsList(ViewModel viewModel) {
    final size = MediaQuery.of(context).size;
    final ratio = (size.height * 10) / (size.width * 10);
    return Container(
      // height: 366,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            FittedBox(
              child: Scatter(
                fillGaps: false,
                delegate: ArchimedeanSpiralScatterDelegate(
                  ratio: 366 / 1200,
                ),
                children:
                    List.generate(viewModel.interestList!.interests.length, (i) {

                      //TODO: Get from Store

                  bool isSelected = viewModel
                      .selectedInterests
                      .where((element) =>
                          element.sId == viewModel.interestList!.interests[i].sId)
                      .isNotEmpty;

                  return ScatterItem(
                    interest: viewModel.interestList!.interests[i],
                    isSelected: isSelected,
                    onTap: () {
                      if (isSelected) {
                        //TODO: Call Redux Action
                        viewModel.onRemovePassionsAction!(viewModel.interestList!.interests[i]);
                      } else {
                        //TODO: Call Redux Action
                        viewModel.onAddPassionsAction!(viewModel.interestList!.interests[i]);
                      }
                    },
                  );
                }),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildButton(ThemeData theme, TextTheme textTheme, ViewModel viewModel) {
    return FractionallySizedBox(
      widthFactor: 320 / 414,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3),
            gradient: LinearGradient(colors: [
              AppColors.scaffoldColorLight,
              theme.scaffoldBackgroundColor
            ], begin: Alignment.centerLeft, end: Alignment.centerRight)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: (){
              Navigator.of(context).pushNamed(Routes.profile);
            },
            child: Container(
              height: 53,
              child: Center(
                child: Text(
                  S.continueStr.toUpperCase(),
                  style: textTheme.subtitle2,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}



