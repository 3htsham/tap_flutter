import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_scatter/flutter_scatter.dart';
import 'package:tap/config/app_assets.dart';
import 'package:tap/config/app_colors.dart';
import 'package:tap/config/constants.dart';
import 'package:tap/library/carousel/carousel_pro.dart';
import 'package:tap/src/elements/my_icon_button.dart';
import 'package:tap/src/elements/my_solid_button.dart';
import 'package:tap/src/elements/scatter_circle_item.dart';
import 'package:tap/src/redux/actions.dart';
import 'package:tap/src/redux/models/view_model.dart';
import 'package:tap/src/redux/user_state.dart';

class ProfileWidget extends StatefulWidget {
  const ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      body: StoreConnector<UserState, ViewModel>(
        onInit: (store) => store.dispatch(fetchProfile(store)),
        converter: (store) => ViewModel.create(store),
        builder: (context, viewModel) {
          return Container(
            height: size.height,
            width: size.width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AppAssets.background), fit: BoxFit.cover)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Stack(
                    children: [

                      _buildImageSlider(theme, size, viewModel),

                      _buildLikeDislikeButtons(size, viewModel),
                    ],
                  ),

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 38, vertical: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [

                        Text(
                          '${viewModel.userProfile?.name ?? ''}, ${viewModel.userProfile?.age ?? ''}',
                          style: textTheme.bodyText1,
                        ),

                        SizedBox(height: 17,),

                        Text(
                          S.profileDescription,
                          maxLines: 2,
                          style: textTheme.bodyText1,
                        ),

                        SizedBox(height: 40,),

                        Text(S.basicInfo, style: textTheme.bodyText1,),

                        SizedBox(height: 22,),

                        _buildBasicInfo(textTheme, viewModel),

                        SizedBox(height: 55,),

                        Text(S.personalInfo, style: textTheme.bodyText1,),

                        SizedBox(height: 22,),

                        _buildPersonalInfo(textTheme, viewModel),

                        SizedBox(height: 40,),

                        Text(S.instagramPosts, style: textTheme.bodyText1,),

                        SizedBox(height: 22,),

                        //TODO: Instagram Posts Grid Here
                        Container(),
                        viewModel.userProfile != null && viewModel.userProfile?.media != null && viewModel.userProfile!.media!.length > 0
                            ? _buildInstagramPosts(viewModel)
                            : SizedBox(),

                        SizedBox(height: 40,),

                        Text(S.passions, style: textTheme.bodyText1,),

                      ],
                    ),
                  ),

                  //TODO: Passions/Interests Here
                  viewModel.userProfile != null && viewModel.userProfile?.interests != null && viewModel.userProfile!.interests!.length > 0
                      ? _buildPassions(viewModel)
                      : SizedBox(),

                  SizedBox(height: 34,),

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 38, vertical: 30),
                    child: _buildButtons(viewModel),
                  ),


                  SizedBox(height: 60,),

                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildImageSlider(ThemeData theme, Size size, ViewModel viewModel){
    return viewModel.userProfile != null &&
        viewModel.userProfile!.photos != null &&
        viewModel.userProfile!.photos!.length > 0
        ? Container(
      width: size.width,
      height: size.height * 0.71,
      child: Carousel(
        autoplay: false,
        boxFit: BoxFit.fill,
        dotColor: Colors.white,
        dotSize: 7,
        dotIncreaseSize: 1.5,
        dotSpacing: 20,
        dotBgColor: Colors.transparent,
        showIndicator: true,
        overlayShadow: false,
        banners: viewModel.userProfile!.photos,
      ),
    )
        : Container(
      width: size.width,
      height: size.height * 0.71,
      color: theme.scaffoldBackgroundColor,
    );
  }

  Widget _buildLikeDislikeButtons(Size size, ViewModel viewModel){
    return Positioned(
      right: 28,
      top: size.height * 0.38,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MyIconButton(
          AppAssets.heartIcon,
              18,
              onTap: (){},),
          SizedBox(height: 22,),
          MyIconButton(
            AppAssets.dislikeIcon,
            22,
            onTap: (){},),
        ],
      ),
    );
  }

  Widget _buildBasicInfo(TextTheme textTheme, ViewModel viewModel) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(17),
      child: Container(
        color: AppColors().focusColor(0.6),
        child: Column(
          children: [

            Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(S.name, style: textTheme.bodyText1,),
                  Text('${viewModel.userProfile?.name ?? ''}', style: textTheme.bodyText1,),
                ],
              ),
            ),

            Divider(thickness: 1, height: 1,),

            Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(S.gender, style: textTheme.bodyText1,),
                  Text('${viewModel.userProfile?.gender ?? ''}', style: textTheme.bodyText1,),
                ],
              ),
            ),

            Divider(thickness: 1, height: 1,),

            Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(S.age, style: textTheme.bodyText1,),
                  Text('${viewModel.userProfile?.age ?? ''}', style: textTheme.bodyText1,),
                ],
              ),
            ),

            Divider(thickness: 1, height: 1,),

            Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(S.location, style: textTheme.bodyText1,),
                  Text('${viewModel.userProfile?.location?.type ?? ''}', style: textTheme.bodyText1,),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }

  Widget _buildPersonalInfo(TextTheme textTheme, ViewModel viewModel) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(17),
      child: Container(
        color: AppColors().focusColor(0.6),
        child: Column(
          children: [

            viewModel.userProfile != null && viewModel.userProfile!.basicInfo != null && viewModel.userProfile!.basicInfo!.length > 0
             ? ListView.separated(
              shrinkWrap: true,
              primary: false,
              itemCount: viewModel.userProfile?.basicInfo?.length ?? 0,
              padding: EdgeInsets.all(0),
              itemBuilder: (context, i) {
                final inf = viewModel.userProfile!.basicInfo![i];
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('${inf.key?.name ?? ''}', style: textTheme.bodyText1,),
                      Text('${inf.value}', style: textTheme.bodyText1,),
                    ],
                  ),
                );
              },
              separatorBuilder: (context, i) {
                return Divider(thickness: 1, height: 1,);
              },
            )
            : SizedBox(),

          ],
        ),
      ),
    );
  }

  Widget _buildInstagramPosts(ViewModel viewModel){
    // final posts = this.userProfile!.media!;
    return Container(
      height: 230,
      child: GridView.count(
        crossAxisCount: 3,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
        padding: EdgeInsets.all(0),
        children: List.generate(9, (index) {
          return ClipRRect(
            borderRadius: BorderRadius.circular(13),
            child: Container(
              color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.5),
            ),
          );
        }),
      ),
    );
  }

  List<Widget> widgets = <Widget>[];
  Widget _buildPassions(ViewModel viewModel){

    final count = viewModel.userProfile!.interests!.length - 1;
    widgets.clear();
    for (var i = 0; i < count; i++) {
      widgets.add(ScatterCircleItem(i, viewModel.userProfile!.interests![i]));
    }

    return Container(
      height: 350,
      child: Stack(
        children: [
          Center(
            child: Scatter(
              delegate: EllipseScatterDelegate(
                a: 120.0,
                b: 120.0,
                step: 1.0 / count,
              ),
              children: widgets,
            ),
          ),

          Align(
            alignment: Alignment.center,
              child: ScatterCircleItem(viewModel.userProfile!.interests!.length, viewModel.userProfile!.interests!.last)),
        ],
      ),
    );
  }

  Widget _buildButtons(ViewModel viewModel) {
    return Column(
      children: [

        MySolidButton(
          text: S.report,
          onTap: (){
            //TODO: Implement Report Functionality Here
          },
          color: AppColors.buttonDark,
        ),
        SizedBox(height: 12,),

        MySolidButton(
          text: S.unPair,
          onTap: (){
            //TODO: Implement UnPair Functionality Here
          },
          color: AppColors.buttonSemiDark.withOpacity(0.35),
        ),
        SizedBox(height: 12,),

        MySolidButton(
          text: S.block,
          onTap: (){
            //TODO: Implement Block Functionality Here
          },
          color: AppColors.buttonLight.withOpacity(0.35),
        ),
      ],
    );
  }

}


