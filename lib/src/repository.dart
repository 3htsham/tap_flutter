import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:tap/src/models/user_profile.dart';

import 'helper/helper.dart';
import 'models/interest_list.dart';

Future<InterestList?> getInterestsListFuture() async {
  InterestList? interestList;
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}v1/category/60597fa4eb72561fb6bb064f';

  final client = new http.Client();

  final response = await client.get(
    Uri.parse(url),
  );

  if (response.statusCode == 200) {
    var body = response.body;
    final res = Helper.getCategoriesData(json.decode(body));

    if(res != null) {
      interestList = InterestList.fromJson(res);
    }
  }
  return interestList;

}

Future<UserProfile?> getUserProfile(
    {String token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxMmRkNWVjNGVkMmUzODFiZWIxYjQ1ZSIsImlhdCI6MTYzMDM5Mzg0MywiZXhwIjoxNjM4MTY5ODQzfQ.ABQ1TlIlg3kybAwTtvKmS8Uq42ThHKYO5D1726ixtxk'}) async {
  UserProfile? _userProfile;
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}v1/profile/me';

  Map<String, String> headers = Map<String, String>();
  headers['Authorization'] = 'Bearer $token';
  headers[HttpHeaders.contentTypeHeader] = 'application/json';

  final client = new http.Client();

  final response = await client.get(
    Uri.parse(url),
    headers: headers,
  );

  if (response.statusCode == 200) {
    var body = response.body;
    final res = Helper.getProfileData(json.decode(body));

    if(res != null) {
      _userProfile = UserProfile.fromJson(res);
    }
  }
  return _userProfile;
}
