import 'package:redux/redux.dart';
import 'package:tap/src/models/interest.dart';
import 'package:tap/src/models/interest_list.dart';
import 'package:tap/src/models/user_profile.dart';

import '../actions.dart';
import '../user_state.dart';

class ViewModel {


  InterestList? interestList;

  List<Interest> selectedInterests = [];

  UserProfile? userProfile;

  Function(InterestList list)? onPassionsSuccessAction;
  Function(UserProfile profile)? onProfileSuccessAction;
  Function(Interest interest)? onAddPassionsAction;
  Function(Interest interest)? onRemovePassionsAction;

  ViewModel({
    this.interestList,
    this.userProfile,
    this.onAddPassionsAction,
    this.onPassionsSuccessAction,
    this.onProfileSuccessAction,
    this.onRemovePassionsAction,
  });


  factory ViewModel.create(Store<UserState> store) {

    _onPassionsSuccessAction(InterestList _list){
      store.dispatch(PassionsSuccessAction(_list));
    }

    _onProfileSuccessAction(UserProfile _profile){
      store.dispatch(ProfileSuccessAction(_profile));
    }

    _onAddPassionsAction(Interest _interest){
      store.dispatch(AddPassionsAction(_interest));
    }

    _onRemovePassionsAction(Interest _interest){
      store.dispatch(RemovePassionsAction(_interest));
    }


    var model = ViewModel(
      interestList: store.state.interestList,
      userProfile: store.state.userProfile,
      onAddPassionsAction: _onAddPassionsAction,
      onPassionsSuccessAction: _onPassionsSuccessAction,
      onProfileSuccessAction: _onProfileSuccessAction,
      onRemovePassionsAction: _onRemovePassionsAction
    );
    model.selectedInterests = store.state.selectedInterests;
    return model;
  }




}