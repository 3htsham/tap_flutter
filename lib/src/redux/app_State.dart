import 'package:flutter/cupertino.dart';
import 'package:tap/src/redux/user_state.dart';

class AppState {

  UserState? userState;

  AppState({@required this.userState});

  factory AppState.initialState() {
    return AppState(userState: UserState());
  }

  AppState copyWith({UserState? userState}) {
    return AppState(userState: userState ?? this.userState);
  }

  @override
  int get hashCode => userState.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState && userState == other.userState;


}