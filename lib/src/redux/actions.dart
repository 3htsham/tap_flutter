import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:tap/src/models/interest.dart';
import 'package:tap/src/models/interest_list.dart';
import 'package:tap/src/models/user_profile.dart';
import 'package:tap/src/redux/user_state.dart';
import 'package:tap/src/repository.dart' as repo;


Future<void> fetchPassions(Store<UserState> store) async {
  try {
    repo.getInterestsListFuture().then((value) {
      if(value != null) {
        store.dispatch(PassionsSuccessAction(value));
      }
    });
  } catch (e) {
    print(e);
  }
}

Future<void> fetchProfile(Store<UserState> store) async {
  try {
    repo.getUserProfile().then((value) {
      if(value != null) {
        store.dispatch(ProfileSuccessAction(value));
      }
    });
  } catch (e) {
    print(e);
  }
}

class PassionsSuccessAction {
  InterestList? interestList;
  PassionsSuccessAction(this.interestList);
}

class ProfileSuccessAction {
  UserProfile? userProfile;
  ProfileSuccessAction(this.userProfile);
}

class AddPassionsAction {
  Interest interest;
  AddPassionsAction(this.interest);
}

class RemovePassionsAction {
  Interest interest;
  RemovePassionsAction(this.interest);
}