import 'package:tap/src/models/interest.dart';
import 'package:tap/src/models/interest_list.dart';
import 'package:tap/src/models/user_profile.dart';

class UserState {


  InterestList? interestList;

  List<Interest> selectedInterests = [];

  UserProfile? userProfile;

  UserState({
    this.interestList,
    this.userProfile
  });

  factory UserState.initialState() {
    return UserState();
  }

  UserState copyWith({InterestList? interestList, List<Interest>? selectedInterests, UserProfile? profile}) {
    var newState = UserState(
      interestList: interestList ?? this.interestList,
      userProfile: profile ?? this.userProfile
    );
    if(selectedInterests != null) {
      newState.selectedInterests = selectedInterests;
    }
    return newState;
  }

  @override
  int get hashCode => interestList.hashCode ^ userProfile.hashCode ^ selectedInterests.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserState &&
        runtimeType == other.runtimeType &&
        interestList == other.interestList &&
        selectedInterests == other.selectedInterests &&
        userProfile == other.userProfile;


}