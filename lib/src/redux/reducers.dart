import 'package:redux/redux.dart';
import 'package:tap/src/redux/user_state.dart';

import 'actions.dart';

final appReducers = combineReducers<UserState>([
  TypedReducer<UserState, ProfileSuccessAction>(_profileSuccessAction),
  TypedReducer<UserState, PassionsSuccessAction>(_passionsSuccessAction),
  TypedReducer<UserState, AddPassionsAction>(_addPassionAction),
  TypedReducer<UserState, RemovePassionsAction>(_removePassionAction),
]);

UserState _profileSuccessAction(UserState state, ProfileSuccessAction profileSuccessAction){
  return state.copyWith(profile: profileSuccessAction.userProfile);
}

UserState _passionsSuccessAction(UserState state, PassionsSuccessAction passionsSuccessAction){
  return state.copyWith(interestList: passionsSuccessAction.interestList);
}

UserState _addPassionAction(UserState state, AddPassionsAction addPassionsAction){
  bool isSelected = state
      .selectedInterests
      .where((element) =>
  element.sId == addPassionsAction.interest.sId)
      .isNotEmpty;

  if(!isSelected) {
  state.selectedInterests
      .add(addPassionsAction.interest);
  }

  return state.copyWith(selectedInterests: state.selectedInterests);
}

UserState _removePassionAction(UserState state, RemovePassionsAction removePassionsAction){
  state.selectedInterests.removeWhere((element) =>
  element.sId ==
      removePassionsAction.interest.sId);
  return state.copyWith(selectedInterests: state.selectedInterests);
}