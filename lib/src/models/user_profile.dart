import 'photo.dart';
import 'basic_info.dart';
import 'filter.dart';
import 'interest.dart';
import 'media.dart';
import 'user_location.dart';

class UserProfile {
  late UserLocation? location;
  late String name;
  late String profilePicture;
  late String gender;
  late int age;
  late String socketId;
  late String friendshipSocketId;
  late String bio;
  late String customerId;
  late String subscriptionId;
  late List<Null>? instagram;
  late bool isBoosted;

  late bool isBreak;

  late int count;
  late String boostExp;
  late bool boostPaid;
  late String premiumExp;
  late Null isGlobal;
  late bool isDating;
  late String jobTitle;
  late List<String>? likes;
  late List<Null>? friendshipLikes;
  late List<Null>? friendshipDislikes;
  late List<Null>? blocked;
  late List<Null>? dislikes;
  late List<Interest>? interests;
  late bool isPremium;
  late String currency;
  late String sId;
  late List<BasicInfo>? basicInfo;
  late List<Filter>? filters;
  late String birthday;
  late String user;
  late List<Photo>? photos;
  late List<Null>? preference;
  late String date;
  late int iV;
  late List<Media>? media;
  late String id;

  UserProfile();

  UserProfile.fromJson(Map<String, dynamic> json) {
    location = json['location'] != null
        ? new UserLocation.fromJson(json['location'])
        : null;
    name = json['name'];
    profilePicture = json['profile_picture'];
    gender = json['gender'];
    age = json['age'];
    socketId = json['socket_id'];
    friendshipSocketId = json['friendship_socket_id'];
    bio = json['bio'];
    customerId = json['customer_id'];
    subscriptionId = json['subscription_id'];
    if (json['instagram'] != null) {
      instagram = [];
      json['instagram'].forEach((v) {
        // instagram.add(new Null.fromJson(v));
      });
    }
    isBoosted = json['is_boosted'];
    isBreak = json['break'];
    count = json['count'];
    boostExp = json['boost_exp'];
    boostPaid = json['boost_paid'];
    premiumExp = json['premium_exp'];
    isGlobal = json['is_global'];
    isDating = json['is_dating'];
    jobTitle = json['job_title'];
    likes = json['likes'].cast<String>();
    if (json['friendship_likes'] != null) {
      friendshipLikes = [];
      json['friendship_likes'].forEach((v) {
        // friendshipLikes.add(new Null.fromJson(v));
      });
    }
    if (json['friendship_dislikes'] != null) {
      friendshipDislikes = [];
      json['friendship_dislikes'].forEach((v) {
        // friendshipDislikes.add(new Null.fromJson(v));
      });
    }
    if (json['blocked'] != null) {
      blocked = [];
      json['blocked'].forEach((v) {
        // blocked.add(new Null.fromJson(v));
      });
    }
    if (json['dislikes'] != null) {
      dislikes = [];
      json['dislikes'].forEach((v) {
        // dislikes.add(new Null.fromJson(v));
      });
    }
    if (json['interests'] != null) {
      interests = [];
      json['interests'].forEach((v) {
        interests!.add(new Interest.fromJson(v));
      });
    }
    isPremium = json['is_premium'];
    currency = json['currency'];
    sId = json['_id'];
    if (json['basic_info'] != null) {
      basicInfo = [];
      json['basic_info'].forEach((v) {
        basicInfo!.add(new BasicInfo.fromJson(v));
      });
    }
    if (json['filters'] != null) {
      filters = [];
      json['filters'].forEach((v) {
        filters!.add(new Filter.fromJson(v));
      });
    }
    birthday = json['birthday'];
    user = json['user'];
    if (json['photos'] != null) {
      photos = [];
      json['photos'].forEach((v) {
        photos!.add(new Photo.fromJson(v));
      });
    }
    if (json['preference'] != null) {
      preference = [];
      json['preference'].forEach((v) {
        // preference.add(new Null.fromJson(v));
      });
    }
    date = json['date'];
    iV = json['__v'];
    if (json['media'] != null) {
      media = [];
      json['media'].forEach((v) {
        media!.add(new Media.fromJson(v));
      });
    }
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.location != null) {
      data['location'] = this.location!.toJson();
    }
    data['name'] = this.name;
    data['profile_picture'] = this.profilePicture;
    data['gender'] = this.gender;
    data['age'] = this.age;
    data['socket_id'] = this.socketId;
    data['friendship_socket_id'] = this.friendshipSocketId;
    data['bio'] = this.bio;
    data['customer_id'] = this.customerId;
    data['subscription_id'] = this.subscriptionId;
    if (this.instagram != null) {
      // data['instagram'] = this.instagram.map((v) => v.toJson()).toList();
    }
    data['is_boosted'] = this.isBoosted;
    data['break'] = this.isBreak;
    data['count'] = this.count;
    data['boost_exp'] = this.boostExp;
    data['boost_paid'] = this.boostPaid;
    data['premium_exp'] = this.premiumExp;
    data['is_global'] = this.isGlobal;
    data['is_dating'] = this.isDating;
    data['job_title'] = this.jobTitle;
    data['likes'] = this.likes;
    if (this.friendshipLikes != null) {
      // data['friendship_likes'] = this.friendshipLikes!.map((v) => v.toJson()).toList();
    }
    if (this.friendshipDislikes != null) {
      // data['friendship_dislikes'] = this.friendshipDislikes!.map((v) => v.toJson()).toList();
    }
    if (this.blocked != null) {
      // data['blocked'] = this.blocked!.map((v) => v.toJson()).toList();
    }
    if (this.dislikes != null) {
      // data['dislikes'] = this.dislikes!.map((v) => v.toJson()).toList();
    }
    if (this.interests != null) {
      data['interests'] = this.interests!.map((v) => v.toJson()).toList();
    }
    data['is_premium'] = this.isPremium;
    data['currency'] = this.currency;
    data['_id'] = this.sId;
    if (this.basicInfo != null) {
      data['basic_info'] = this.basicInfo!.map((v) => v.toJson()).toList();
    }
    if (this.filters != null) {
      data['filters'] = this.filters!.map((v) => v.toJson()).toList();
    }
    data['birthday'] = this.birthday;
    data['user'] = this.user;
    if (this.photos != null) {
      data['photos'] = this.photos!.map((v) => v.toJson()).toList();
    }
    if (this.preference != null) {
      // data['preference'] = this.preference!.map((v) => v.toJson()).toList();
    }
    data['date'] = this.date;
    data['__v'] = this.iV;
    if (this.media != null) {
      data['media'] = this.media!.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    return data;
  }
}
