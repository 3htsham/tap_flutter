import 'info_key.dart';

class BasicInfo {
  late bool premium;
  late String sId;
  late InfoKey? key;
  late String value;

  BasicInfo({this.premium = false, this.sId = '', this.key, this.value = ''});

  BasicInfo.fromJson(Map<String, dynamic> json) {
    premium = json['premium'];
    sId = json['_id'];
    key = json['key'] != null ? new InfoKey.fromJson(json['key']) : null;
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['premium'] = this.premium;
    data['_id'] = this.sId;
    if (this.key != null) {
      data['key'] = this.key!.toJson();
    }
    data['value'] = this.value;
    return data;
  }
}


