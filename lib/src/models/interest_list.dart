import 'package:tap/src/models/interest.dart';

class InterestList {
  late List<Interest> interests = [];
  late bool premium;
  late String sId;
  late String name;
  late String selection;
  late String date;
  late String slug;
  late int iV;

  InterestList(
      {
        this.premium = false,
        this.sId = '',
        this.name = '',
        this.selection = '',
        this.date = '',
        this.slug = '',
        this.iV = 0});

  InterestList.fromJson(Map<String, dynamic> json) {
    if (json['interests'] != null) {
      interests = [];
      json['interests'].forEach((v) {
        interests.add(new Interest.fromJson(v));
      });
    }
    premium = json['premium'];
    sId = json['_id'];
    name = json['name'];
    selection = json['selection'];
    date = json['date'];
    slug = json['slug'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.interests != null) {
      data['interests'] = this.interests.map((v) => v.toJson()).toList();
    }
    data['premium'] = this.premium;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['selection'] = this.selection;
    data['date'] = this.date;
    data['slug'] = this.slug;
    data['__v'] = this.iV;
    return data;
  }
}