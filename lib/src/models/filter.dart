import 'info_key.dart';

class Filter {
  late bool premium;
  late String sId;
  late InfoKey? key;
  late String value;
  late String type;
  late String selection;
  late String range;

  Filter(
      {this.premium = false,
        this.sId = '',
        this.key,
        this.value = '',
        this.type = '',
        this.selection = '',
        this.range = ''});

  Filter.fromJson(Map<String, dynamic> json) {
    premium = json['premium'];
    sId = json['_id'];
    key = json['key'] != null ? new InfoKey.fromJson(json['key']) : null;
    value = json['value'];
    type = json['type'];
    selection = json['selection'];
    range = json['range'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['premium'] = this.premium;
    data['_id'] = this.sId;
    if (this.key != null) {
      data['key'] = this.key!.toJson();
    }
    data['value'] = this.value;
    data['type'] = this.type;
    data['selection'] = this.selection;
    data['range'] = this.range;
    return data;
  }
}