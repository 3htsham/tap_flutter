class Photo {
  late String filename;
  late String comment;
  late int index;
  late bool isVideo;
  late String sId;

  Photo({this.filename = '', this.comment = '', this.index = 0, this.isVideo = false,
  this.sId = ''});

  Photo.fromJson(Map<String, dynamic> json) {
    filename = json['filename'];
    comment = json['comment'];
    index = json['index'];
    isVideo = json['is_video'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['filename'] = this.filename;
    data['comment'] = this.comment;
    data['index'] = this.index;
    data['is_video'] = this.isVideo;
    data['_id'] = this.sId;
    return data;
  }
}
