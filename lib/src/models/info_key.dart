class InfoKey {
  late var sId;
  late var name;
  late var selection;

  InfoKey({this.sId = '', this.name = '', this.selection = ''});

  InfoKey.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    selection = json['selection'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['selection'] = this.selection;
    return data;
  }
}