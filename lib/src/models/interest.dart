import 'package:tap/src/models/category.dart';

class Interest {
  late bool flag;
  late String sId;
  late String name;
  late var category;
  late String date;
  late String slug;
  late int iV;
  late String image;

  Interest(
      {this.flag = false,
        this.sId = '',
        this.name = '',
        this.category = '',
        this.date = '',
        this.slug = '',
        this.iV = 0,
        this.image = ''});

  Interest.fromJson(Map<String, dynamic> json) {
    flag = json['flag'];
    sId = json['_id'];
    name = json['name'];
    category = json['category'] != null
        ? (json['category'] is String)
          ? json['category']
          : Category.fromJson(json['category'])
        : null;
    date = json['date'];
    slug = json['slug'];
    iV = json['__v'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['flag'] = this.flag;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['category'] = this.category != null
        ? this.category is String
          ? this.category
          : this.category.toJson()
        : null;
    data['date'] = this.date;
    data['slug'] = this.slug;
    data['__v'] = this.iV;
    data['image'] = this.image;
    return data;
  }
}