class Media {
  late List<Question> question = [];
  late bool isVideo;
  late var sId;
  late var video;
  late var user;
  late var date;
  late var iV;

  Media();

  Media.fromJson(Map<String, dynamic> json) {
    if (json['question'] != null) {
      question = [];
      json['question'].forEach((v) {
        question.add(new Question.fromJson(v));
      });
    }
    isVideo = json['is_video'];
    sId = json['_id'];
    video = json['video'];
    user = json['user'];
    date = json['date'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.question != null) {
      data['question'] = this.question.map((v) => v.toJson()).toList();
    }
    data['is_video'] = this.isVideo;
    data['_id'] = this.sId;
    data['video'] = this.video;
    data['user'] = this.user;
    data['date'] = this.date;
    data['__v'] = this.iV;
    return data;
  }
}

class Question {
  late bool flag;
  late var sId;
  late var name;
  late var category;
  late var date;
  late var slug;
  late var iV;

  Question();

  Question.fromJson(Map<String, dynamic> json) {
    flag = json['flag'];
    sId = json['_id'];
    name = json['name'];
    category = json['category'];
    date = json['date'];
    slug = json['slug'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['flag'] = this.flag;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['category'] = this.category;
    data['date'] = this.date;
    data['slug'] = this.slug;
    data['__v'] = this.iV;
    return data;
  }
}
